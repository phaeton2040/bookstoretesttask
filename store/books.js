module.exports = [
    {
        title: 'Создание приложений на Android для начинающих',
        year: '2015',
        authors: [
            'Майк Макграт'
        ],
        pageAmount: 192,
        publisher: 'Эксмо',
        isbn: '978-5-699-81145-8',
        image: '1013807685.jpg'

    },
    {
        title: 'Чистый код: создание, анализ и рефакторинг. Библиотека программиста',
        year: '2016',
        authors: [
            'Роберт К. Мартин'
        ],
        pageAmount: 464,
        publisher: 'Питер',
        isbn: '978-5-496-00487-9',
        image: '1007098845.jpg'
    },
    {
        title: 'Программист-фанатик',
        year: '2016',
        authors: [
            'Майк Макграт'
        ],
        pageAmount: 208,
        publisher: 'Питер',
        isbn: '	978-5-496-01062-7',
        image: '1011960574.jpg'
    },
    {
        title: 'Hello World! Занимательное программирование',
        year: '2016',
        authors: [
            'Картер Сэнд', 'Уоррен Сэнд'
        ],
        pageAmount: 400,
        publisher: 'Питер',
        isbn: '978-5-496-01273-7',
        image: '1013897053.jpg'
    },
    {
        title: 'C++ для чайников',
        year: '2003',
        authors: [
            'Стефан Рэнди Дэвис', 'Уоррен Сэнд'
        ],
        pageAmount: 400,
        publisher: 'Вильямс',
        isbn: '978-5-8459-1952-6',
        image: '99d03d2cbb49f4795e37d7093865de13_1012024090.jpg'
    },
    {
        title: 'Как управлять вселенной не привлекая внимания',
        year: '1980',
        authors: [
            'И. Кутузов'
        ],
        pageAmount: 235,
        publisher: 'НИИ Пандора',
        isbn: '978-1-118-82377-4',
        image: '3faff44dfb52034b8cfa9a82913e3cb8_4998405-R3L8T8D-400-1.jpg'
    },
    {
        title: 'Создаем динамические веб-сайты с помощью PHP, MySQL, JavaScript, CSS и HTML5',
        year: '2015',
        authors: [
            'Робин Никсон'
        ],
        pageAmount: 768,
        publisher: 'Питер',
        isbn: '978-5-496-02146-3',
        image: '1014783247.jpg'
    },
    {
        title: 'Офисные решения с использованием Microsoft Excel 2007 и VBA',
        year: '2007',
        authors: [
            'Сергей Кашаев'
        ],
        pageAmount: 205,
        publisher: 'Питер',
        isbn: '978-5-699-81145-8',
        image: '1010506411.jpg'
    },
    {
        title: 'Turbo Pascal 7.0. Самоучитель для начинающих',
        year: '2002',
        authors: [
            'Сергей Лукин'
        ],
        pageAmount: 384,
        publisher: 'Диалог-МИФИ',
        isbn: '5-86404-122-X',
        image: '1004458309.jpg'
    },
    {
        title: 'Язык Java и Microsoft Visual J++ 6.0 в действии',
        year: '2005',
        authors: [
            'Тахмасиб Дадашев'
        ],
        pageAmount: 382,
        publisher: 'Горячая Линия - Телеком',
        isbn: '5-93517-065-5',
        image: '1004512458.jpg'
    }
];