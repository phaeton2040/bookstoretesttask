import React, {Component} from 'react';
import {Decorator as FormsyElement} from 'formsy-react';
import ControlLabel from 'react-bootstrap/lib/ControlLabel';
import FormGroup from 'react-bootstrap/lib/FormGroup';
import $ from 'jquery';

@FormsyElement()
class ValidatedFileInput extends React.Component {

    onFileAdd = (event) => {
        const files = event.target.files;
        const self = this;

        if (files.length > 0) {

            // создаем объект, который будем передавать ajax запросом
            var formData = new FormData();

            // по заданию обложка у книги всего одна, но возможно когда нибудь
            // нужно будет загружать несколько изображений
            for (var i = 0; i < files.length; i++) {
                var file = files[i];

                formData.append('image', file, file.name);
            }

            $.ajax({
                url: '/upload',
                type: 'POST',
                data: formData,
                processData: false,
                contentType: false,
                success: function(data){
                    const image = data.filename;

                    self.props.setValue(image)
                },
                xhr: function() { // в этом методе мы будем отслеживать прогресс загрузки
                    var xhr = new XMLHttpRequest();
                    $('.progress-bar').text('0%');
                    $('.progress-bar').width('0%');

                    // Подписываемся на событие 'progress'
                    xhr.upload.addEventListener('progress', function(evt) {

                        if (evt.lengthComputable) {
                            // считаем процент загрузки

                            var percentComplete = evt.loaded / evt.total;
                            percentComplete = parseInt(percentComplete * 100);
                            //
                            // обновляем полосу загрузки в соответствии с переданными данными
                            $('.progress-bar').text(percentComplete + '%');
                            $('.progress-bar').width(percentComplete + '%');
                            //
                            if (percentComplete === 100) {
                                $('.progress-bar').html('Загружено');
                            }
                        }

                    }, false);

                    return xhr;
                }
            });
        }
    }

    render() {
        return (
        <FormGroup controlId={this.props.name}>
            <ControlLabel>{this.props.desc}:</ControlLabel>
            <input type="file" onChange={this.onFileAdd} name={this.props.name} className="form-control"/>
        </FormGroup>
        );
    }
};
export default ValidatedFileInput