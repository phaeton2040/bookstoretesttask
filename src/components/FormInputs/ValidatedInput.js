import React, {Component} from 'react';
import {Decorator as FormsyElement} from 'formsy-react';
import ControlLabel from 'react-bootstrap/lib/ControlLabel';
import FormGroup from 'react-bootstrap/lib/FormGroup';

@FormsyElement()
class ValidatedInput extends React.Component {
    render() {
        const isInitial = this.props.getValue() === undefined;

        return (
            <FormGroup controlId={this.props.name} className={this.props.isValid() ? 'success' : !this.props.isValid() && !isInitial ? 'error' : ''}>
                <ControlLabel>{this.props.desc}</ControlLabel>
                <input type="text" data-field={this.props.name} value={this.props.getValue()} onChange={(e) => this.props.setValue(e.target.value)} className="form-control"/>
                <span className="error-text">{this.props.getErrorMessage()}</span>
            </FormGroup>
        );
    }
};
export default ValidatedInput