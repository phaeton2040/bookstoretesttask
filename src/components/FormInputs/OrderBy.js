import React, {Component} from 'react';
import _ from 'underscore';
import ControlLabel from 'react-bootstrap/lib/ControlLabel';
import FormGroup from 'react-bootstrap/lib/FormGroup';

class OrderBy extends Component{
    constructor(props){
        super(props);
    }

    onChange = (event) => {
        const order = event.target.value;

        if (localStorage) {
            localStorage.setItem('order', order);
        }

        this.props.sortBooks(order);
    }

    render(){
        const {fields, order} = this.props;

        return(
            <div className="book-filter-order">
                <FormGroup controlId="orderBy">
                    <ControlLabel>Выберите сортировку:</ControlLabel>
                    <select className='form-control' value={order} placeholder="Сортировать по" onChange={this.onChange}>
                        {
                            _.map(fields, field => {
                                return (
                                    <option
                                        key={`order_${field[0]}`}
                                        value={field[0]}
                                    >
                                        {field[1]}
                                    </option>
                                )
                            })
                        }
                    </select>
                </FormGroup>
            </div>
        )
    }
}

export default OrderBy