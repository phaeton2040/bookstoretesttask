import React, {Component} from 'react';
import ControlLabel from 'react-bootstrap/lib/ControlLabel';
import FormGroup from 'react-bootstrap/lib/FormGroup';

class OrderBy extends Component{
    constructor(props){
        super(props);
    }


    onChange = (event) => {
        const query = event.target.value;

        this.props.textSearch(query);
    }

    render(){
        const {query} = this.props;

        return(
            <div className="book-filter-order">
                <FormGroup controlId="orderBy">
                    <ControlLabel>Поиск по названию или автору:</ControlLabel>
                    <input className='form-control' value={query} placeholder="Поиск" onChange={this.onChange} />
                </FormGroup>
            </div>
        )
    }
}

export default OrderBy;