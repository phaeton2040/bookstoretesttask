import React, {Component} from 'react';
import _ from 'underscore';

class Book extends Component{
    constructor(props){
        super(props);
    }

    getAuthors = () => {
        const {book} = this.props;
        const {authors} = book;

        return authors.join(', ');
    }

    onClick = () => {
        const {id} = this.props;

        this.props.changeBook(parseInt(id.split('_')[1]));
    }

    render(){
        const {book, current} = this.props;

        return(
            <div className={`book-list-item ${current ? 'current' : ''}`} onClick={this.onClick}>
                <div className="book-list-item-title">
                    {book.title}
                </div>
                <div className="book-list-item-meta">
                    <div className="book-list-item-author">
                        {this.getAuthors()}, {book.year} г.
                    </div>
                </div>
            </div>
        )
    }
}

export default Book