import React, {Component} from 'react';
import FormGroup from 'react-bootstrap/lib/FormGroup';
import ValidatedInput from '../FormInputs/ValidatedInput';
import ValidatedFileInput from '../FormInputs/ValidatedFileInput';
import Button from 'react-bootstrap/lib/Button';
import Formsy from 'formsy-react';
import Modal from 'react-bootstrap/lib/Modal';
import _ from 'underscore';

Formsy.addValidationRule('isMoreThan', function (values, value, arg) {
    return Number(value) > Number(arg);
});

Formsy.addValidationRule('Exists', function (values, value) {
    return !!value;
});

Formsy.addValidationRule('validAuthors', function (values, value, arg) {
    if (!value) return false;
    let valid = false;

    const authors = value.split(',');
    valid = true;

    _.each(authors, author => {
        if (author.length > arg) {
            valid = false;
        }
    })


    return valid;
});

Formsy.addValidationRule('isIsbn', function (values, value) {
    const isbn10DigitsRegexp = RegExp(/^(?:ISBN(?:-10)?:?\ )?(?=[0-9X]{10}$|(?=(?:[0-9]+[-\ ]){3})[-\ 0-9X]{13}$)[0-9]{1,5}[-\ ]?[0-9]+[-\ ]?[0-9]+[-\ ]?[0-9X]$/);
    const isbn13DigitsRegexp = RegExp(/^(?:ISBN(?:-13)?:?\ )?(?=[0-9]{13}$|(?=(?:[0-9]+[-\ ]){4})[-\ 0-9]{17}$)97[89][-\ ]?[0-9]{1,5}[-\ ]?[0-9]+[-\ ]?[0-9]+[-\ ]?[0-9]$/);

    return isbn10DigitsRegexp.test(value) || isbn13DigitsRegexp.test(value);
});

class BookForm extends Component{
    constructor(props){
        super(props);

        this.state = {
            showModal: false,
            form: {}
        }
    }

    setBook = (props) => {
        const book = props.book ? _.clone(props.book) : {};

        if (props.book) {
            book.authors = book.authors.join(', ');
        }

        this.setState(() => ({
            form: book
        }))
    }

    componentWillReceiveProps(nextProps) {
        this.setBook(nextProps);
    }

    componentWillMount() {
        this.setBook(this.props);
    }

    toggle = () => {
        this.setState(() => ({
            showModal: !this.state.showModal
        }))
    }

    onSubmit = (form) => {
        this.props.addBook(form);

        this.setState(() => ({
            showModal: false,
            form: {
                title: '',
                year: '',
                isbn: '',
                pageAmount: '',
                publisher: '',
                authors: ''
            }
        }));
    }

    enableButton = () => {
        this.setState(() => ({
            canSubmit: true
        }));
    }

    disableButton = () => {
        this.setState(() => ({
            canSubmit: false
        }));
    }

    render() {

        const {form} = this.state;

        return(
            <div className={this.props.book ? 'book-filter-form-edit' : 'book-filter-form'}>
                <Button
                    bsStyle={this.props.book ? 'default' : 'primary'}
                    onClick={this.toggle}
                >
                    {
                        this.props.book ? 'Редактировать' : 'Добавить новую книгу'
                    }
                </Button>

                <Modal show={this.state.showModal} onHide={this.toggle}>
                    <Modal.Header closeButton>
                        <Modal.Title>Добавить новую книгу</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Formsy.Form ref="bookForm" onValidSubmit={this.onSubmit} onValid={this.enableButton} onInvalid={this.disableButton}>
                            <ValidatedInput name="title" desc="Название книги" value={form.title} validations="Exists,maxLength:50" validationErrors={{
                                Exists: '',
                                maxLength: 'Превышено максимальное кол-во символов'
                            }} />
                            <ValidatedInput name="year" desc="Год издания" value={form.year} validations="Exists,isMoreThan:1800" validationErrors={{
                                Exists: '',
                                isMoreThan: 'Не раньше 1800 года'
                            }}/>
                            <ValidatedInput name="publisher" desc="Издатель" value={form.publisher} validations="Exists,maxLength:50" validationErrors={{
                                Exists: '',
                                maxLength: 'Превышено максимальное кол-во символов'
                            }}/>
                            <ValidatedInput name="isbn" desc="ISBN" value={form.isbn} validations="Exists,isIsbn" validationErrors={{
                                Exists: '',
                                isIsbn: 'Неправильный ISBN'
                            }}/>
                            <ValidatedInput name="pageAmount" desc="Кол-во страниц" value={form.pageAmount} validations="Exists,isMoreThan:0" validationErrors={{
                                Exists: '',
                                isMoreThan: 'Кол-во страниц задано неверно'
                            }}/>
                            <ValidatedInput name="authors" desc="Автор (если несколько, то вводить через запятую)" value={form.authors} validations="Exists,validAuthors:30" validationErrors={{
                                Exists: '',
                                validAuthors: 'Кол-во символов превышено для одного или нескольких авторов'
                            }}/>
                            <ValidatedFileInput name="image" desc="Изображение" value={form.image} onFileAdd={this.onFileAdd}/>
                            <div className="progress">
                                <div className="progress-bar" role="progressbar"></div>
                            </div>
                            <FormGroup>
                                <input type="submit" value="Добавить" disabled={!this.state.canSubmit} className="btn btn-primary"/>
                            </FormGroup>
                        </Formsy.Form>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button onClick={this.toggle}>Закрыть</Button>
                    </Modal.Footer>
                </Modal>
            </div>
        )
    }
}

export default BookForm