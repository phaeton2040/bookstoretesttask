import React, {Component} from 'react';
import BookForm from './BookForm';

class BookInfo extends Component{
    constructor(props){
        super(props);
    }

    onDelClick(event) {
        event.preventDefault();

        this.props.deleteBook(this.props.book);
    }

    render(){
        const {book} = this.props;
        const image = book.image ? book.image : 'no-photo.png';

        if (!book) {
            return null;
        }

        return(
            <div className="book-single-wrapper">
                <div className="book-single-image">
                    <img src={`/uploads/${image}`} alt={book.title}/>
                    <BookForm book={book} addBook={this.props.addBook} />
                    <a href="#" onClick={this.onDelClick.bind(this)} className="btn btn-danger">Удалить</a>
                </div>
                <div className="book-single-description">
                    <div className="book-single-description-item">
                        <b>Название: </b> {book.title}
                    </div>
                    <div className="book-single-description-item">
                        <b>Автор(ы): </b> {book.authors.join(', ')}
                    </div>
                    <div className="book-single-description-item">
                        <b>Издатель: </b> {book.publisher}
                    </div>
                    <div className="book-single-description-item">
                        <b>Кол-во страниц: </b> {book.pageAmount}
                    </div>
                    <div className="book-single-description-item">
                        <b>ISBN: </b> {book.isbn}
                    </div>
                    <div className="book-single-description-item">
                        <b>Год: </b> {book.year}
                    </div>
                </div>
            </div>
        )
    }
}

export default BookInfo;