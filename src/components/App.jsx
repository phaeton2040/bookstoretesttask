import React, {Component} from 'react';
import Row from 'react-bootstrap/lib/Row';
import Col from 'react-bootstrap/lib/Col';
import GeminiScrollbar from 'react-gemini-scrollbar';
import $ from 'jquery';
import _ from 'underscore'
import Book from './Book/Book';
import BookInfo from './Book/BookInfo';
import OrderBy from './FormInputs/OrderBy';
import TextSearch from './FormInputs/TextSearch';
import BookForm from './Book/BookForm';

class App extends Component{
	constructor(props){
		super(props);
		this.state = {
		    order: localStorage && localStorage.getItem('order') ?
                localStorage.getItem('order') : 'title'
        }
	}

	componentWillMount() {
        const self = this;
        //получаем список книг перед отрисовкой компонента
        $.ajax({
            url: '/books',
            dataType: 'json',
            type: 'get',
            success: function(response) {
                self.setState(() => ({
                    books: _.sortBy(response, book => {
                        return book[self.state.order]
                    }),
                    currentBookKey: 0
                }))
            }
        })
	}

	// переключение между книгами
	changeBook(id) {
        this.setState(() => ({
            currentBookKey: id
        }))
    }

    // сортировка книг. order - название св-ва из одиночного объекта книги
    sortBooks(order) {
        const {books, filteredBooks} = this.state;
        const orderedBooks = _.sortBy(filteredBooks ? filteredBooks : books, book => {
            return book[order];
        })

        this.setState(() => ({
            filteredBooks: orderedBooks,
            order: order
        }));
    }

    // текстовый поиск
    textSearch(query) {
        const filteredBooksByTitle = _.filter(this.state.books, book => {
            return book.title.toLowerCase().indexOf(query.toLowerCase()) !== -1;
        });
        const filteredBooksByAuthor= _.filter(this.state.books, book => {
            const authors = book.authors.join('');

            return authors.toLowerCase().indexOf(query.toLowerCase()) !== -1;
        });

        this.setState(() => ({
            filteredBooks: _.union(filteredBooksByAuthor, filteredBooksByTitle)
        }));
    }

    addBook(formData) {
        const self = this;
        formData.authors = formData.authors.split(',');
        formData.authors = _.map(formData.authors, author => author.trim())

        $.ajax({
            url: '/addBook',
            type: 'post',
            data: formData,
            success: (response) => {
                const sortedBooks = _.sortBy(response, (book, key)=> {
                    return book[self.state.order]
                });
                const newBookKey = _.map(sortedBooks, book => book.title).indexOf(formData.title);

                self.setState(() => ({
                    books: sortedBooks,
                    currentBookKey: newBookKey
                }))
            }
        })
    }

    deleteBook(book) {
        if (confirm('Удалить книгу из списка?')) {
            const self = this;

            $.ajax({
                url: '/deleteBook',
                type: 'post',
                data: book,
                success: (response) => {
                    console.dir(response);
                    const sortedBooks = _.sortBy(response, (book)=> {
                        return book[self.state.order]
                    });

                    self.setState(() => ({
                        books: sortedBooks,
                        currentBookKey: 0
                    }))
                }
            });
        }
    }

	render(){

		if (!this.state.books) {
			return null;
		}

		const {books, currentBookKey, filteredBooks} = this.state;
		const currentBook = filteredBooks ?
            filteredBooks[currentBookKey] ?
                filteredBooks[currentBookKey] :
                filteredBooks[0] :
            books[currentBookKey];

		return(
			<div className="container">
                <Row>
                    <Col md={4}>
                        <OrderBy
                            order={this.state.order}
                            sortBooks={this.sortBooks.bind(this)}
                            fields={[
                                [
                                    'year', 'Год'
                                ],
                                [
                                    'pageAmount', 'Кол-во страниц'
                                ],
                                [
                                    'title', 'Название'
                                ]
                            ]}
                        />
                        <TextSearch query={this.state.query} textSearch={this.textSearch.bind(this)} />
                    </Col>
                </Row>
				<Row>
					<Col md={4} xs={6} className="books-list-wrapper">
                        <GeminiScrollbar>
                            {
                                _.map(filteredBooks ? filteredBooks: books, (book, key) => {
                                    return (
                                        <Book book={book}
                                              key={`book_${key}`}
                                              id={`book_${key}`}
                                              changeBook={this.changeBook.bind(this)}
                                              current={key === currentBookKey} />
                                    )
                                })
                            }
                        </GeminiScrollbar>
					</Col>
					<Col md={8} xs={6}>
                        {
                            <div>
                                <BookForm addBook={this.addBook.bind(this)} />
                                <BookInfo book={currentBook} deleteBook={this.deleteBook.bind(this)} addBook={this.addBook.bind(this)} />
                            </div>
                        }
					</Col>
				</Row>
			</div>
		)
	}
}

export default App