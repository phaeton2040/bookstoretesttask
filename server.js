var express = require('express');
var app = express();
var path = require('path');
var multer  = require('multer')
var fs = require('fs');
var parser = require('body-parser');
var books = require('./store/books');
var _ = require('underscore');

var upload = multer({ dest: 'public/uploads/tmp'});

app.use(express.static(path.join(__dirname, 'public')));
app.use(parser.urlencoded({extended: true}));

// отдаем главную и единственную страницу
app.get('/', function(req, res){
    res.sendFile(path.join(__dirname, 'views/index.html'));
});

app.post('/addBook', function(req, res) {
    var newBook = req.body;

    // считаем, что isbn - уникальный идентификатор для книги
    var existingIdx = _.findLastIndex(books, {isbn: newBook.isbn});

    if (existingIdx !== -1) {
        books[existingIdx] = newBook;
    } else {
        books.push(newBook);
    }

    res.json(books);
});

app.post('/deleteBook', function(req, res) {
    var book = req.body;
    var existingIdx = _.findLastIndex(books, {isbn: book.isbn});

    if (books[existingIdx]) {
        books.splice(existingIdx, 1);
    }

    res.json(books);
});

app.post('/upload', upload.single('image'), function(req, res){

    var file = __dirname + '/public/uploads/' + req.file.filename + '_' + req.file.originalname;

    fs.rename(req.file.path, file, function(err) {
        if (err) {
            console.log(err);
            res.sendStatus(500);
            res.end();
        } else {
            res.json({
                success: true,
                filename: req.file.filename + '_' + req.file.originalname
            });
        }
    });

});

app.get('/books', function(req, res) {
    console.dir('Books are requested from: ' + req.ip);
    res.json(books);
});

app.listen(3000, function(){
    console.log('Server listening on port 3000');
});